# UNET

[Based on the Unity website material](https://docs.unity3d.com/Manual/UNet.html)

## NetworkManager
- **features**: Game State Management, Spawn Management, Scene Management, Debugging Information, Matchmaking, Customization

- only one active per scene

- needs **Player Prefab** to function properly (check **Spawn Management**). When the game running, NetworkManager creates an instance of the prefab for each player that connects to the match. Can be set runtime

- don't put **NetworkIdentity** to the object which has **NetworkManager** because Unity disables **NetworkIdentity** objects on scene load


### Game State Management
- three possible states: **dedicated server, host, client**

- **(dedicated) server**: Needed to have a multiplazer game, has no client

	`NetworkManager.StartServer()`

- **host**: a server which also is a client

	`NetworkManager.StartHost()`

- **client**: connects to a **server** or a **host**.

	`NetworkManager.StartClient()`

- **NetworkAddress**: IP address, used to communicate regardless of game state

- **NetworkPort**: communication (connect, listen..) goes through this port regardless of game state


### Spawn Management (Spawn Info)

**Spawning**: network instantiation of **networked GameObjects** (derivatives of `NetworkBehaviour`) from prefabs.

- **Player Prefab**: assign the player prefab. Can be set at runtime but you may need to rewrite `OnServerAddPlayer()`. **Player Prefab** needs to have `NetworkIdentity` and probably `NetworkTransform`. Players instantiated from this prefab on host and client as well. 

- Host gets a spawned player at startup. When client connects, receives a player instance. When disconnects its player instance gets destroyed.

- **Registered Spawnable Prefabs**: any object dynamically spawned should have a prefab here otherwise spawning won't happen.


#### Customizing Player Instantiation

`NetworkManager.OnServerAddPlayer()` can be overridden. instantiate the GameObject and call `NetworkManager.AddPlayerForConnection()` to spawn. Don't use `NetworkServer.Spawn()` here.

#### Starting Positions (NetworkStartPosition)

`NetworkStartPosition` a component. It can be used to set the starting position where the spawning puts down the player. Attach to a GameObject and position where the player should spawn. Any number of start position is allowed.

- NetworkManager has `PlayerSpawnMethod` property which allows to configure how start positions are chosen (random / round robin).

- If customization needed the available `NetworkStartPosition`s are available in `NetworkManager.startPositions`. Also `NetworkManager.GetStartPosition()` can be used in implementation of `NetworkManager.OnServerAddPlayer()` to find a start position.

### Scene Management

dragging scenes into the following two activates networking scene management. 

- **Online scene**: when a server or host started, this online scene gets loaded. Any client connecting to the server is instructed to load this scene. The name of the scene is stored in the `networkSceneName` property

- **Offline scene**: when the network is stopped, by stopping the server or host or by a client disconnecting, the offline scene gets loaded. This allows the game to return to a menu scene when disconnected from a multiplayer game.

- `NetworkManager.ServerChangeScene()`: call to change scenes while the game is active. This makes **all the currently connected clients** change scene too and updates the `networkSceneName` so that new clients also load the new scene.
 
 - all the GameObject gets destroyed on scene change


### Customization

It is possible to derive from `NetworkManager` and overwrite its virtual methods:

- `StartHost()`:

- `ServerChangeScene()`:

- `OnServerConnect()`: a client connected to the server

- `OnServerDisconnect()`: a  client disconnected from the server, could be an error

- `OnServerReady()`: client is set to the ready state (ready to receive state updates)

- `OnServerAddPlayer()`: client has requested to get his player added to the game

- `OnServerRemovePlayer()`:

- `OnServerError()`: server network error occurred

- `OnServerSceneChanged()`: server triggering a scene change. Will trigger `OnClientSceneChanged()` for clients

- `OnClientConnect()`: connecting to server, set up other stuff for the client after completion..

- `OnClientDisconnect()`: disconnect client from server

- `OnClientError()`: client network error occurred

- `OnClientNotReady()`: server setting client to be not-ready (stop getting state updates)

- `OnClientSceneChanged()`: server triggered scene change in `OnServerSceneChanged()` and we've doing the same, do any extra work here for the client after...

- `OnStartHost()`: starting as a Host

- `OnStartServer()`: starting as a dedicated server

- `OnStartClient()`: starting as a client

- `OnStopServer()`: stopping dedicated server

- `OnStopClient()`: stopping the client

- `OnStopHost()`: stoppint the Host

- `OnMatchCreate()`:

- `OnMatchList()`:


## NetworkManagerHUD

- basic UI, only for testing when there is no final UI implemented yet

## Networked Player

- make it a **prefab**

- prefab should be dragged into NetworkManager

- the scripts should be "aware" of wheter the player controlling the instance is using the **host** or a **client** computer















